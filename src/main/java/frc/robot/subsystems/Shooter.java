// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.sensors.SensorVelocityMeasPeriod;

import edu.wpi.first.util.sendable.SendableRegistry;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.shooter.ShooterDoNothing;

public class Shooter extends SubsystemBase {
  /** Creates a new Shooter. */
  private static WPI_TalonFX frontShooterMotor = new WPI_TalonFX(Constants.SHOOTER_WHEEL_FRONT_MOTOR_ID);
  private static WPI_TalonFX rearShooterMotor = new WPI_TalonFX(Constants.SHOOTER_REAR_WHEEL_MOTOR_ID);
  public Shooter() {
    setDefaultCommand(new ShooterDoNothing(this));
    frontShooterMotor.configFactoryDefault();
    rearShooterMotor.configFactoryDefault();
    frontShooterMotor.setNeutralMode(NeutralMode.Coast);
    rearShooterMotor.setNeutralMode(NeutralMode.Coast);

    SendableRegistry.setName(frontShooterMotor, "ShooterSub", "Front Shooter Motor");
    SendableRegistry.setName(rearShooterMotor, "ShooterSub", "Rear Shooter Motor");

    /*
        RobotIO.shooterMotor3.configSelectedFeedbackCoefficient( 2.0/1024.0, 0, RobotSettings.CAN_TIMEOUT_INTERVAL);
    RobotIO.shooterMotor3.config_kP(RobotSettings.SHOOTER_SLOT, RobotSettings.SHOOTER_KP, RobotSettings.CAN_TIMEOUT_INTERVAL);
    RobotIO.shooterMotor3.config_kI(RobotSettings.SHOOTER_SLOT, RobotSettings.SHOOTER_KI, RobotSettings.CAN_TIMEOUT_INTERVAL);
    RobotIO.shooterMotor3.config_kD(RobotSettings.SHOOTER_SLOT, RobotSettings.SHOOTER_KD, RobotSettings.CAN_TIMEOUT_INTERVAL);
    RobotIO.shooterMotor3.config_kF(RobotSettings.SHOOTER_SLOT, RobotSettings.SHOOTER_KF, RobotSettings.CAN_TIMEOUT_INTERVAL);
     
    */
    frontShooterMotor.config_kP(0, Constants.SHOOTER_kP);
    frontShooterMotor.config_kI(0, Constants.SHOOTER_kI);
    frontShooterMotor.config_kF(0, Constants.SHOOTER_kF);
    frontShooterMotor.config_kD(0, Constants.SHOOTER_kD);
    
    rearShooterMotor.config_kF(0, Constants.SHOOTER_kF);
    rearShooterMotor.config_kI(0, Constants.SHOOTER_kI);
    rearShooterMotor.config_kD(0, Constants.SHOOTER_kD);
    rearShooterMotor.config_kP(0, Constants.SHOOTER_kP);

    frontShooterMotor.configVoltageCompSaturation(12);
    rearShooterMotor.configVoltageCompSaturation(12);
    frontShooterMotor.enableVoltageCompensation(true);
    rearShooterMotor.enableVoltageCompensation(true);

    frontShooterMotor.configVelocityMeasurementPeriod(SensorVelocityMeasPeriod.Period_10Ms);
    rearShooterMotor.configVelocityMeasurementPeriod(SensorVelocityMeasPeriod.Period_10Ms);

  }

  private static Shooter shooter;

  public static Shooter getInstance() {
    if(shooter == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      shooter = new Shooter();
    }
    return shooter;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    if (DriverStation.isDisabled()) { return; }
    SmartDashboard.putNumber("Front RPM", Shooter.getFrontRPM());
    SmartDashboard.putNumber("Rear RPM", Shooter.getBackRPM());
  }

  public static void shoot(double front, double back) {
    frontShooterMotor.set(front);
    rearShooterMotor.set(back);
  }


  public static void setRPM(int frontRPM, int backRPM) {
    frontShooterMotor.set(ControlMode.Velocity, frontRPM);
    rearShooterMotor.set(ControlMode.Velocity, backRPM);
  }

  public static double getFrontRPM() {
    return frontShooterMotor.getSelectedSensorVelocity();
  }
  public static double getBackRPM() {
    return rearShooterMotor.getSelectedSensorVelocity();
  }

}
