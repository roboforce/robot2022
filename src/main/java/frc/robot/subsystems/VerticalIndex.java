// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.indexer.VerticalIndexDoNothing;

public class VerticalIndex extends SubsystemBase {
  /** Creates a new VerticalIndex. */

  public static CANSparkMax motor2 = new CANSparkMax(Constants.INDEXER_MOTOR_VERTICAL_ID, MotorType.kBrushless);  

  private static VerticalIndex verticalIndex;

  public static VerticalIndex getInstance() {
    if(verticalIndex == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      verticalIndex = new VerticalIndex();
    }
    return verticalIndex;
  }

  public VerticalIndex() {
    setDefaultCommand(new VerticalIndexDoNothing(this));
    motor2.setInverted(true);
    motor2.setIdleMode(IdleMode.kBrake);
  }

  public static void setSpeed(double speed) {
    motor2.set(speed);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
