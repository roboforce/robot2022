// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.indexer.HorizontalDoNothing;

public class HorizontalIndex extends SubsystemBase {
  /** Creates a new HorizontalIndex. */

  public static CANSparkMax motor1 = new CANSparkMax(Constants.INDEXER_MOTOR_HORIZONTAL_ID, MotorType.kBrushless);  

  private static HorizontalIndex horizontalIndex;

  public static HorizontalIndex getInstance() {
    if(horizontalIndex == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      horizontalIndex = new HorizontalIndex();
    }
    return horizontalIndex;
  }

  public HorizontalIndex() {
    setDefaultCommand(new HorizontalDoNothing(this));

  }

  public static void setSpeed(double speed) {
    motor1.set(speed);
  }

  public static void run() {
    setSpeed(Constants.HORIZONTAL_INDEX_SPEED);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
