// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.intakearm.ArmDoNothing;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

public class IntakeArm extends SubsystemBase {
  //Digital switches
  public static DigitalInput switchLower = new DigitalInput(Constants.INTAKE_ARM_SWITCH_LOWER_ID);
  public static DigitalInput switchUpper = new DigitalInput(Constants.INTAKE_ARM_SWITCH_UPPER_ID);

  //Main motor
  public static CANSparkMax intakeArmMotor = new CANSparkMax(Constants.INTAKE_ARM_MOTOR_ID, MotorType.kBrushless);

  /** Creates a new IntakeArm. */
  public IntakeArm() {
    setDefaultCommand(new ArmDoNothing(this));
    intakeArmMotor.restoreFactoryDefaults();
    IntakeArm.intakeArmMotor.getEncoder().setPositionConversionFactor(Constants.INTAKE_ARM_MOTOR_CONVERSION_FACTOR);
    intakeArmMotor.setIdleMode(IdleMode.kBrake);
    intakeArmMotor.setSmartCurrentLimit(50);
  }

  private static IntakeArm intakeArm;

  public static IntakeArm getInstance() {
    if(intakeArm == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      intakeArm = new IntakeArm();
    }
    return intakeArm;
  }

  public static boolean armIsUp() {
    double height = IntakeArm.getArmPosition();
    if (height < -0.05) {
      IntakeArm.intakeArmMotor.getEncoder().setPosition(-0.05);
    }
    return IntakeArm.getArmPosition() <= 0 || intakeArmMotor.getOutputCurrent() > 100;
  }

  public static boolean armIsDown() {
    return IntakeArm.getArmPosition() >= 1;
  }

  public static void setSpeed(double speed) {
    if (intakeArmMotor.getMotorTemperature() > 60) {
      intakeArmMotor.set(0);
      return;
    }
    if (speed < 0 && armIsUp()) {
      intakeArmMotor.set(0);
      return;
    }
    if (speed > 0 && armIsDown()) {
      intakeArmMotor.set(0);
      return;
    }
    intakeArmMotor.set(speed);
  }

  public static void armUp(){
      setSpeed(Constants.INTAKE_ARM_MOTOR_SPEED);
  }

  public static void armDown(){
      setSpeed(Constants.INTAKE_ARM_MOTOR_DOWN_SPEED);
  }
  
  public static double getArmPosition() {
    return IntakeArm.intakeArmMotor.getEncoder().getPosition();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Pulses", IntakeArm.getArmPosition());
    SmartDashboard.putBoolean("Arm Up", armIsUp());
    SmartDashboard.putBoolean("Arm Down", armIsDown());
    if (DriverStation.isDisabled()) { return; }
    SmartDashboard.putNumber("Arm Speed", intakeArmMotor.get());
    SmartDashboard.putNumber("Arm Current", intakeArmMotor.getOutputCurrent());
    SmartDashboard.putNumber("Intake Temp", intakeArmMotor.getMotorTemperature());
  }


}
