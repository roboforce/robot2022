// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
//change

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.intakeWheels.IntakeDoNothing;

public class IntakeWheels extends SubsystemBase {
  /** Creates a new IntakeWheels. */
  private static CANSparkMax intakeMotor = new CANSparkMax(Constants.INTAKE_WHEEL_MOTOR_ID, MotorType.kBrushless);
  public IntakeWheels() {
    setDefaultCommand(new IntakeDoNothing(this));

  } 

  private static IntakeWheels intakeWheels;

  public static IntakeWheels getInstance() {
    if(intakeWheels == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      intakeWheels = new IntakeWheels();
    }
    return intakeWheels;
  }

  @Override
  public void periodic() {

    // This method will be called once per scheduler run
  }

  public static void setIntakeSpeed(double speed) {
    intakeMotor.set(speed);
  }

  
}
