// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.util.sendable.SendableRegistry;
import edu.wpi.first.wpilibj.ADIS16448_IMU;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.ADIS16448_IMU.IMUAxis;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.drivetrain.DriveWithJoystick;

public class Drivetrain extends SubsystemBase {

  //individual motors
  public static final WPI_TalonFX leftMoter1 = new WPI_TalonFX(Constants.LEFT_DRIVE_MOTER_FRONT_ID);
  public static final WPI_TalonFX leftMoter2 = new WPI_TalonFX(Constants.LEFT_DRIVE_MOTER_REAR_ID);
  public static final WPI_TalonFX rightMoter1 = new WPI_TalonFX(Constants.RIGHT_DRIVE_MOTER_FRONT_ID);
  public static final WPI_TalonFX rightMoter2 = new WPI_TalonFX(Constants.RIGHT_DRIVE_MOTER_REAR_ID);

  //differential drive
  public static DifferentialDrive drivetrain = new DifferentialDrive(leftMoter1, rightMoter1);

  public static DifferentialDriveKinematics drivetrainKinematics = new DifferentialDriveKinematics(Constants.K_TRACK_WIDTH_METERS);

  public static DifferentialDriveOdometry drivetrainOdemetry;

  //distance variables
  private static double inchesTraveled = 0;

  //gyro
  public static final ADIS16448_IMU gyro = new ADIS16448_IMU();


  /** Creates a new Drivetrain. */
  public Drivetrain() {
    setDefaultCommand(new DriveWithJoystick(this));

    //set up config
    leftMoter1.configFactoryDefault();
    leftMoter2.configFactoryDefault();
    rightMoter1.configFactoryDefault();
    rightMoter2.configFactoryDefault();    

    //set the back motors to follow the front
    leftMoter2.follow(leftMoter1);
    leftMoter2.setStatusFramePeriod(StatusFrame.Status_1_General, 255);
    leftMoter2.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, 255);
    rightMoter2.follow(rightMoter1);
    rightMoter2.setStatusFramePeriod(StatusFrame.Status_1_General, 255);
    rightMoter2.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, 255);

    //invert encoders to properly calculate distance values
    rightMoter2.setInverted(true);
    rightMoter1.setInverted(true);

    //set up current limitter
    StatorCurrentLimitConfiguration currentLimitConfiguration = new StatorCurrentLimitConfiguration(true, 90, 100, 0.1);
    SupplyCurrentLimitConfiguration currentSupplyConfiguration = new SupplyCurrentLimitConfiguration(true, 90, 100, 0.1);

    leftMoter2.configSupplyCurrentLimit(currentSupplyConfiguration);
    leftMoter1.configSupplyCurrentLimit(currentSupplyConfiguration);
    rightMoter1.configSupplyCurrentLimit(currentSupplyConfiguration);
    rightMoter2.configSupplyCurrentLimit(currentSupplyConfiguration);

    leftMoter1.configStatorCurrentLimit(currentLimitConfiguration);
    rightMoter1.configStatorCurrentLimit(currentLimitConfiguration);
    rightMoter2.configStatorCurrentLimit(currentLimitConfiguration);
    leftMoter2.configStatorCurrentLimit(currentLimitConfiguration);
    

    gyro.setYawAxis(IMUAxis.kX);

    SendableRegistry.setName(rightMoter1, "DriveSub", "Right Motor 1");
    SendableRegistry.setName(rightMoter2, "DriveSub", "Right Motor 2");
    SendableRegistry.setName(leftMoter1, "DriveSub", "Left Motor 1");
    SendableRegistry.setName(leftMoter2, "DriveSub", "Left Motor 2");

    SendableRegistry.setName(gyro, "DriveSub", "Gyro");
    drivetrainOdemetry = new DifferentialDriveOdometry(new Rotation2d(0));
    coastMode();
  }

  public static void breakMode() {
    leftMoter1.setNeutralMode(NeutralMode.Brake);
    rightMoter2.setNeutralMode(NeutralMode.Brake);
    rightMoter1.setNeutralMode(NeutralMode.Brake);
    leftMoter2.setNeutralMode(NeutralMode.Brake);

  }

  public static void coastMode() {
    leftMoter1.setNeutralMode(NeutralMode.Coast);
    rightMoter2.setNeutralMode(NeutralMode.Coast);
    rightMoter1.setNeutralMode(NeutralMode.Coast);
    leftMoter2.setNeutralMode(NeutralMode.Coast);
  }

  private static Drivetrain instance;

  public static Drivetrain getInstance() {
    if (instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new Drivetrain();
    }
    return instance;
  }

  
  double maxSpeed = 0;

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    if (DriverStation.isDisabled()) { return; }
    drivetrainOdemetry.update(new Rotation2d(getAngle()), leftDistanceMovedMeters(), rightDistanceMovedMeters());
    SmartDashboard.putNumber("Left Distance", leftDistanceMovedMeters());
    SmartDashboard.putNumber("Right Distance", rightDistanceMovedMeters());
    SmartDashboard.putNumber("Gyro", getAngle());

    maxSpeed = Math.max(maxSpeed, (leftMoter1.getSelectedSensorVelocity() + rightMoter1.getSelectedSensorVelocity()) / 2);
    SmartDashboard.putNumber("Max Speed Reached", maxSpeed);
  }

  public void resetOdometry(Pose2d pose) {
    resetAngle();
    resetDistance();
    Rotation2d temp = new Rotation2d(gyro.getAngle());
    drivetrainOdemetry.resetPosition(pose, temp);
  }

  public static DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(
      Math.abs(leftMoter1.getSelectedSensorVelocity()*Constants.INCHES_PER_PULSE*Constants.METERS_PER_INCH), 
      Math.abs(rightMoter1.getSelectedSensorVelocity()*Constants.INCHES_PER_PULSE*Constants.METERS_PER_INCH));
  }

  public static void tankDriveVolts(double leftVolts, double rightVolts) {
    leftMoter1.setVoltage(leftVolts);
    rightMoter1.setVoltage(rightVolts);
    drivetrain.feed();
  }

  public static Pose2d getPose() {
    return drivetrainOdemetry.getPoseMeters();
  }

  public double getHeading() {
    return gyro.getAngle();
  }

  public static double getAngle() {
    return -gyro.getAngle();
  }

  public static void resetAngle() {
    gyro.reset();
  }

  public static void setSpeed(double speed) {
    rightMoter1.set(speed);
    leftMoter1.set(speed);
  }

  public static double rightDistanceMovedMeters() {
    return (rightMoter1.getSelectedSensorPosition()*Constants.INCHES_PER_PULSE*Constants.METERS_PER_INCH);
  }

  public static double leftDistanceMovedMeters() {
    return (leftMoter1.getSelectedSensorPosition()*Constants.INCHES_PER_PULSE*Constants.METERS_PER_INCH);
  }


  public static double distanceMoved() {
    double pulses = (leftMoter1.getSelectedSensorPosition() + rightMoter1.getSelectedSensorPosition())/2;
    inchesTraveled = (pulses * (Constants.INCHES_PER_PULSE));

    return inchesTraveled;
  }

  public static void resetDistance() {
    inchesTraveled = 0;
    leftMoter1.setSelectedSensorPosition(0);
    leftMoter2.setSelectedSensorPosition(0);
    rightMoter1.setSelectedSensorPosition(0);
    rightMoter2.setSelectedSensorPosition(0);
  }

  public static void arcadeDrive(double speed, double rotation) {
    drivetrain.arcadeDrive(speed, rotation);
  }

  
}
