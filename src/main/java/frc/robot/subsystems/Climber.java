// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.commands.climber.ClimberDoNothing;

public class Climber extends SubsystemBase {
  /** Creates a new Climber. */

  public static CANSparkMax left = new CANSparkMax(Constants.CLIMBER_LEFT_ID, MotorType.kBrushless);
  public static CANSparkMax right = new CANSparkMax(Constants.CLIMBER_RIGHT_ID, MotorType.kBrushless);

  private static Climber instance;

  public static Climber getInstance() {
    if(instance == null) {
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      instance = new Climber();
    }
    return instance;
  }

  public Climber() {
    setDefaultCommand(new ClimberDoNothing(this));
    left.setIdleMode(IdleMode.kBrake);
    right.setIdleMode(IdleMode.kBrake);
    right.getEncoder().setPositionConversionFactor(1/89.0 / 0.85 * 0.98);
    left.getEncoder().setPositionConversionFactor(1/89.0 / 0.85 * 0.96);
    right.setInverted(true);
    left.setInverted(false);
  }

  public static void  setSpeedRaw(double leftSpeed, double rightSpeed) {
    left.set(leftSpeed);
    right.set(rightSpeed);
  }

  public static void setSpeed(double speed) {

    SmartDashboard.putNumber("speed", speed);

    if(speed < 0 && getRightCLimberPosition() <= 0) {
      right.set(0);
    } else if(speed > 0 && getRightCLimberPosition() >= 1) {
      right.set(0);
    } else {
      right.set(speed);
    }
    
    if(speed < 0 & getLeftClimberPosition() <= 0) {
      left.set(0);
    } else if(speed > 0 && getLeftClimberPosition() >= 1) {
      left.set(0);
    } else {
      left.set(speed);
    }

  }

  public static double getRightCLimberPosition() {
    if (right.getEncoder().getPosition() < -0.02) {
      right.getEncoder().setPosition(-0.02);
    }
    return right.getEncoder().getPosition();
  }

  public static double getLeftClimberPosition() {
    if (left.getEncoder().getPosition() < -0.02) {
      left.getEncoder().setPosition(-0.02);
    }
    return left.getEncoder().getPosition();
  }


  @Override
  public void periodic() {
    if (DriverStation.isDisabled()) { return; }
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("left position", left.getEncoder().getPosition());
    SmartDashboard.putNumber("right position", right.getEncoder().getPosition());

  }
}
