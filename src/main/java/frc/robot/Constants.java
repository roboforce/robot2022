// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    //unit calculations
    public static final double INCHES_PER_PULSE = (11.0/50.0)*(20/54.0)*Math.PI*6.25/2048.0 * 0.8;
    public static final double METERS_PER_INCH = 0.0254;
    public static final double INTAKE_ARM_MOTOR_CONVERSION_FACTOR = 1/112.4/0.88;

    //pathweaver values
    public static final double KS_VOLTS = 0.326;
    public static final double KV_VOLTS_SECONDS_PER_METER = 1.52;
    public static final double KA_VOLTS_SECONDS_SQUARED_PER_METER = 0.2;
    public static final double K_TRACK_WIDTH_METERS = 0.561975;
    public static final double K_MAX_SPEED_METERS_PER_SECOND = 3.35;
    public static final double K_MAX_ACCELERATION_METERS_PER_SECOND_SQUARED = 3.35;
    public static final double K_RAMSETE_B = 2;
    public static final double K_RAMSETE_ZETA = 0.7;
    
        

    //motor speeds
    //working shooter values: 5.7 rear, 5.2 front
    //5.2 -> 9620rpm 5.7 -> 105050
    //shooter
    public static final double SHOOTER_HIGH_VOLTS_REAR= 10;
    public static final double SHOOTER_HIGH_VOLTS_FRONT = 10;
    public static final double SHOOTER_HIGH_FAR_VOLTS_REAR= 8;
    public static final double SHOOTER_HIGH_FAR_VOLTS_FRONT = 2.7;
    public static final double SHOOTER_LOW_VOLTS_FRONT = 4.7;
    public static final double SHOOTER_LOW_VOLTS_REAR = 4.7;
    //95, 75 good
    //10, 72.5 decent


    //rear - depth
    //font - height
    //practice match 1 values: 7700 7100

    //match 1: 7500 6700
    public static final int SHOOTER_HIGH_REAR_RPM = 7900;
    public static final int SHOOTER_HIGH_FRONT_RPM = 6700;

    public static final int SHOOTER_DESIRED_RPM_FRONT = 0;
    public static final int SHOOTER_DESIRED_RPM_BACK = 0;

    public static final double SHOOTER_kD = 0;
    public static final double SHOOTER_kF = 0.047;
    //.04
    public static final double SHOOTER_kP = 0.006;
    public static final double SHOOTER_kI = 0;

    //intake wheels
    public static final double INTAKE_SUCK_SPEED = -0.7;

    //intake arm
    public static final int INTAKE_ARM_SWITCH_LOWER_ID = 0; //subject to change
    public static final int INTAKE_ARM_SWITCH_UPPER_ID = 1; //subject to change
    public static final double INTAKE_ARM_MOTOR_SPEED = -0.7; //subject to change
    public static final double INTAKE_ARM_MOTOR_DOWN_SPEED = 0.3;
    public static final double ARM_MOTOR_SPEED_TO_STAY_UP = 0;

    
    //index
    public static final double HORIZONTAL_INDEX_SPEED = 0.7;
    public static final double VERTICAL_INDEX_SPEED = 0.25;
    public static final double VERTICAL_INDEX_SPEED_FAST = 0.45;

    //drivetrain
    public static final double ROTATE_kP = 0.5;
    public static final double ROTATE_kD = .035;
    public static final double ROTATE_kI = 0.01;


    //IDS

    //buttons
    public static final int INTAKE_RAISE_BUTTON_ID = 3;
    public static final int INTAKE_DEPLOY_AND_SPIN_BUTTON_ID = 2;
    public static final int SHOOTER_SHOOT_HIGH_BUTTON_ID = 1;
    public static final int REVERSE_EVERYTHING_BUTTON_ID = 7;
    public static final int INDEX_SPIN_BUTTON_ID = 6;

    //joysticks
    public static final int JOYSTICK_OPERATOR_ID = 1;
    public static final int JOYSTICK_DRIVER_ID = 0;

    //motors
    public static final int RIGHT_DRIVE_MOTER_FRONT_ID = 1;
    public static final int RIGHT_DRIVE_MOTER_REAR_ID = 2;
    public static final int LEFT_DRIVE_MOTER_FRONT_ID = 3;
    public static final int LEFT_DRIVE_MOTER_REAR_ID = 4;
    public static final int INTAKE_WHEEL_MOTOR_ID = 5;
    public static final int INTAKE_ARM_MOTOR_ID = 7;
    public static final int INDEXER_MOTOR_HORIZONTAL_ID = 8; //correct
    public static final int INDEXER_MOTOR_VERTICAL_ID = 9;
    public static final int SHOOTER_WHEEL_FRONT_MOTOR_ID = 10;
    public static final int SHOOTER_REAR_WHEEL_MOTOR_ID = 11;
    public static final int CLIMBER_LEFT_ID = 12;
    public static final int CLIMBER_RIGHT_ID = 13; 
    

    //Auto Distances
    public static final double TAXI_DISTANCE_INCHES = -65;


}
