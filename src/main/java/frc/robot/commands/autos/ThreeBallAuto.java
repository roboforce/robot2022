// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autos;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.commandgroups.WaitShoot;
import frc.robot.commands.drivetrain.DriveDistance;
import frc.robot.commands.drivetrain.TurnAnglePID;
import frc.robot.commands.indexer.HorizontalDoNothing;
import frc.robot.commands.indexer.HorizontalIndexRun;
import frc.robot.commands.indexer.VerticalIndexDoNothing;
import frc.robot.commands.indexer.VerticalIndexRun;
import frc.robot.commands.intakeWheels.SpinIntake;
import frc.robot.commands.intakearm.ArmDown;
import frc.robot.commands.shooter.ShooterDoNothing;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.HorizontalIndex;
import frc.robot.subsystems.IntakeArm;
import frc.robot.subsystems.IntakeWheels;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VerticalIndex;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class ThreeBallAuto extends SequentialCommandGroup {
  /** Creates a new ThreeBallAuto. */
  public ThreeBallAuto() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ParallelRaceGroup(
        new WaitShoot(false),
        new WaitCommand(1.1)
      ),
      new ParallelRaceGroup(
        new ShooterDoNothing(Shooter.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance()),
        new WaitCommand(0.5)
      ),
      new DriveDistance(Drivetrain.getInstance(), -45, -3.5),
      new ParallelRaceGroup(
        new TurnAnglePID(Drivetrain.getInstance(), -90),
        new WaitCommand(1)
      ),
      //new ArmDown(IntakeArm.getInstance()),
      new ParallelRaceGroup(
        new DriveDistance(Drivetrain.getInstance(), 61, 3.5),
        new SpinIntake(IntakeWheels.getInstance()),
        new HorizontalIndexRun(HorizontalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new WaitCommand(.5),
        new HorizontalIndexRun(HorizontalIndex.getInstance()),
        new VerticalIndexRun(VerticalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new HorizontalDoNothing(HorizontalIndex.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance()),
        new WaitCommand(0.1)
      ),
      new ParallelRaceGroup(
        new TurnAnglePID(Drivetrain.getInstance(), -175),
        new WaitCommand(1)
      ),
      //new ArmDown(IntakeArm.getInstance()),
      new ParallelRaceGroup(
        new DriveDistance(Drivetrain.getInstance(), 89, 2.5),
        new HorizontalIndexRun(HorizontalIndex.getInstance()),
        new SpinIntake(IntakeWheels.getInstance())
      ),
      new ParallelRaceGroup(
        new WaitCommand(1),
        new VerticalIndexRun(VerticalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new WaitCommand(0.5),
        new HorizontalDoNothing(HorizontalIndex.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new TurnAnglePID(Drivetrain.getInstance(), -107),
        new WaitCommand(1)
      ),
      new DriveDistance(Drivetrain.getInstance(), 110, 3)
    );
  }
}