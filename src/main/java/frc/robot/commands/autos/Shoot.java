// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autos;


import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.commandgroups.WaitShoot;
import frc.robot.commands.indexer.VerticalIndexDoNothing;
import frc.robot.commands.shooter.ShooterDoNothing;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VerticalIndex;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class Shoot extends SequentialCommandGroup {
  /** Creates a new ShootAtStart. */
  public Shoot() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ParallelRaceGroup(
        new WaitShoot(false),
        new WaitCommand(3)
      ),
      new ParallelRaceGroup(
        new ShooterDoNothing(Shooter.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance()),
        new Taxi()
      )
    );
  }
}
