// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autos;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.commandgroups.WaitShoot;
import frc.robot.commands.drivetrain.DriveDistance;
import frc.robot.commands.drivetrain.TurnAnglePID;
import frc.robot.commands.indexer.HorizontalDoNothing;
import frc.robot.commands.indexer.HorizontalIndexRun;
import frc.robot.commands.indexer.VerticalIndexDoNothing;
import frc.robot.commands.indexer.VerticalIndexRun;
import frc.robot.commands.intakeWheels.IntakeDoNothing;
import frc.robot.commands.intakeWheels.SpinIntake;
import frc.robot.commands.intakearm.ArmDown;
import frc.robot.commands.intakearm.ArmUp;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.HorizontalIndex;
import frc.robot.subsystems.IntakeArm;
import frc.robot.subsystems.IntakeWheels;
import frc.robot.subsystems.VerticalIndex;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class TwoBallAutoTerminalSideWall extends SequentialCommandGroup {
  /** Creates a new TwoBallAuto2. */
  public TwoBallAutoTerminalSideWall() {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ParallelRaceGroup(
        new TurnAnglePID(Drivetrain.getInstance(), 35),
        new WaitCommand(.8) //16/30
      ),
      // new ArmDown(IntakeArm.getInstance()),
      new ParallelRaceGroup(
        new DriveDistance(Drivetrain.getInstance(), 40, 2.5), //70/30
        new SpinIntake(IntakeWheels.getInstance()),
        new VerticalIndexRun(VerticalIndex.getInstance(), true),
        new HorizontalIndexRun(HorizontalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new SpinIntake(IntakeWheels.getInstance()),
        new VerticalIndexRun(VerticalIndex.getInstance(), true),
        new HorizontalIndexRun(HorizontalIndex.getInstance()),
        new WaitCommand(2)
      ),
      new ParallelRaceGroup(
        new IntakeDoNothing(IntakeWheels.getInstance()),
        // new ArmUp(IntakeArm.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance()),
        new HorizontalDoNothing(HorizontalIndex.getInstance())
      ),
      new ParallelRaceGroup(
        new TurnAnglePID(Drivetrain.getInstance(), 156),
        new WaitCommand(1.8) // 40/30
      ),
      new ParallelRaceGroup(
      new DriveDistance(Drivetrain.getInstance(), 135, 4),
      new WaitCommand(2.5) //74/30
      ),
      new WaitShoot(false)
    );
  }
}
