// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.indexer;

import java.util.Currency;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VerticalIndex;

public class VerticalIndexRun extends CommandBase {
  double speed = Constants.VERTICAL_INDEX_SPEED;
  boolean isShooting;
  double maxRpm;
  long time;
  /** Creates a new VerticalIndexRun. */
  public VerticalIndexRun(VerticalIndex verticalIndex) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(verticalIndex);
  }

  public VerticalIndexRun(VerticalIndex verticalIndex, double speed) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(verticalIndex);
    this.speed = speed;
  }

  public VerticalIndexRun(VerticalIndex verticalIndex, boolean shooting) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(verticalIndex);
    this.isShooting = shooting;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    maxRpm = 0;
    time = System.currentTimeMillis();
  }

  int size = 10;
  double[] meas = new double[size];
  int i = 0;

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    i++;
    if(!isShooting) {
      VerticalIndex.setSpeed(speed);
      return;
    }
    double currentBackRPM = Shooter.getBackRPM();
    meas[i%size] = currentBackRPM;
    double total = 99999;
    for(double current : meas) {
      total = Math.min(total, current);
    }

    System.out.println(total - currentBackRPM);
    if(Math.abs(total - currentBackRPM) > 140) time = System.currentTimeMillis();
    if(Math.abs(time - System.currentTimeMillis()) >= 500) { VerticalIndex.setSpeed(Constants.VERTICAL_INDEX_SPEED_FAST); }
    else VerticalIndex.setSpeed(0);
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
