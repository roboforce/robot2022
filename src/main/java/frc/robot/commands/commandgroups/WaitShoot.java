// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.commandgroups;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.indexer.HorizontalIndexRun;
import frc.robot.commands.indexer.VerticalIndexDoNothing;
import frc.robot.commands.indexer.VerticalIndexRun;
import frc.robot.commands.shooter.PIDShooter;
import frc.robot.commands.shooter.ShootHigh;
import frc.robot.subsystems.HorizontalIndex;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VerticalIndex;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class WaitShoot extends SequentialCommandGroup {
  /** Creates a new WaitShoot. */
  public WaitShoot(boolean far) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new ParallelRaceGroup(
        new VerticalIndexRun(VerticalIndex.getInstance(), -0.1),
        new WaitCommand(0.2)
      ),
      new ParallelRaceGroup(
        new PIDShooter(Shooter.getInstance(), Constants.SHOOTER_HIGH_FRONT_RPM, Constants.SHOOTER_HIGH_REAR_RPM),
        new HorizontalIndexRun(HorizontalIndex.getInstance()),
        new VerticalIndexDoNothing(VerticalIndex.getInstance()),
        new WaitCommand(0.5)
      ),
      new ParallelCommandGroup(
        new VerticalIndexRun(VerticalIndex.getInstance(), true),
        new PIDShooter(Shooter.getInstance(), Constants.SHOOTER_HIGH_FRONT_RPM, Constants.SHOOTER_HIGH_REAR_RPM),
        new HorizontalIndexRun(HorizontalIndex.getInstance())
      )
    );
  }
}
