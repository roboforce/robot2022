// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Shooter;

public class ShootHigh extends CommandBase {
  /** Creates a new ShootHigh. */
  boolean far;
  public ShootHigh(Shooter shooter, boolean far) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(shooter);
    this.far = far;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (far){
      Shooter.shoot(Constants.SHOOTER_HIGH_FAR_VOLTS_FRONT, Constants.SHOOTER_HIGH_FAR_VOLTS_REAR);
    } else {
      Shooter.shoot(Constants.SHOOTER_HIGH_VOLTS_FRONT, Constants.SHOOTER_HIGH_VOLTS_REAR);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
