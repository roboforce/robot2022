// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Shooter;

public class PIDShooter extends CommandBase {
  /** Creates a new PIDShooter. */

  private int desiredRPMFront = 0;
  private int desiredRPMBack = 0;

  public PIDShooter(Shooter shooter, int frontRPM, int backRPM) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(shooter);
    desiredRPMFront = frontRPM;
    desiredRPMBack = backRPM;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(desiredRPMBack == 0 || desiredRPMFront == 0) {
      desiredRPMBack = Constants.SHOOTER_DESIRED_RPM_BACK;
      desiredRPMFront = Constants.SHOOTER_DESIRED_RPM_FRONT;
    }
    Shooter.setRPM(desiredRPMFront, desiredRPMBack);
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
