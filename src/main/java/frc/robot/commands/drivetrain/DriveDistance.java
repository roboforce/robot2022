// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class DriveDistance extends CommandBase {

  private double distanceInches;
  private double motorSpeed;

  /** Creates a new DriveDistance. */
  public DriveDistance(Drivetrain drivetrain, double distanceInches, double speed) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(drivetrain);
    this.distanceInches = distanceInches;
    this.motorSpeed = speed;
  }


  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    Drivetrain.resetDistance();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    Drivetrain.tankDriveVolts(motorSpeed, motorSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs( Drivetrain.distanceMoved() )  >= Math.abs( distanceInches );
  }
}
