// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Drivetrain;

public class TurnAnglePID extends CommandBase {

  private double degrees;
  PIDController pid;
  /** Creates a new TurnAngle. */


  public TurnAnglePID(Drivetrain drivetrain, double degrees) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(drivetrain);
    Drivetrain.resetAngle();
    this.degrees = degrees;
    this.pid = new PIDController(Constants.ROTATE_kP, Constants.ROTATE_kI, Constants.ROTATE_kD);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    pid.reset();
    pid.setSetpoint(degrees);
    Drivetrain.resetAngle();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //check if the angle is negative or positive
    double rotationalSpeed = pid.calculate(Drivetrain.getAngle());
    if(rotationalSpeed > 3) rotationalSpeed = 3;
    else if(rotationalSpeed < -3) rotationalSpeed = -3;
    Drivetrain.tankDriveVolts(rotationalSpeed, -rotationalSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    Drivetrain.tankDriveVolts(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
