// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TurnAngleVolts extends CommandBase {

  private double degrees;
  private double speed;
  /** Creates a new TurnAngle. */


  public TurnAngleVolts(Drivetrain drivetrain, double degrees, double speed) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(drivetrain);
    Drivetrain.resetAngle();
    this.degrees = degrees;
    this.speed = speed;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //check if the angle is negative or positive
    Drivetrain.tankDriveVolts(speed, -speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return Math.abs(Drivetrain.getAngle()) >= Math.abs(degrees);
  }
}
