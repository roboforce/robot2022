// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.autos.DriveShoot;
import frc.robot.commands.autos.Shoot;
import frc.robot.commands.autos.ShootFar;
import frc.robot.commands.autos.Taxi;
import frc.robot.commands.autos.ThreeBallAuto;
import frc.robot.commands.autos.TwoBallAutoHangerSide;
import frc.robot.commands.autos.TwoBallAutoTerminalSide;
import frc.robot.commands.autos.TwoBallAutoTerminalSideWall;
import frc.robot.commands.autos.TwoBallAutoHangerSide;
import frc.robot.commands.autos.WaitThanCommand;
import frc.robot.commands.climber.ClimbWhileHeld;
import frc.robot.commands.climber.DeployClimber;
import frc.robot.commands.climber.DeployClimberOnClick;
import frc.robot.commands.climber.ResetClimber;
import frc.robot.commands.commandgroups.GetFromGutter;
import frc.robot.commands.commandgroups.WaitShoot;
import frc.robot.commands.drivetrain.DrivetrainDoNothing;
import frc.robot.commands.drivetrain.TurnAnglePID;
import frc.robot.commands.drivetrain.TurnAngleVolts;
import frc.robot.commands.indexer.HorizontalDoNothing;
import frc.robot.commands.indexer.HorizontalIndexRun;
import frc.robot.commands.indexer.HorizontalSpit;
import frc.robot.commands.indexer.VerticalIndexRun;
import frc.robot.commands.indexer.VerticalIndexSpit;
import frc.robot.commands.intakeWheels.IntakeDoNothing;
import frc.robot.commands.intakeWheels.IntakeWheelSpit;
import frc.robot.commands.intakeWheels.SpinIntake;
import frc.robot.commands.intakearm.ArmDown;
import frc.robot.commands.intakearm.ArmUp;
import frc.robot.commands.shooter.PIDShooter;
import frc.robot.commands.shooter.ShootHigh;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.HorizontalIndex;
import frc.robot.subsystems.IntakeWheels;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VerticalIndex;
import frc.robot.subsystems.IntakeArm;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  Drivetrain drivetrain = Drivetrain.getInstance();
  IntakeWheels intakeWheels = IntakeWheels.getInstance();
  Shooter shooter = Shooter.getInstance();
  IntakeArm intakeArm = IntakeArm.getInstance();
  HorizontalIndex horizontalIndex = HorizontalIndex.getInstance();
  VerticalIndex verticalIndex = VerticalIndex.getInstance();
  Climber climber = Climber.getInstance();

  public static Joystick driverStick = new Joystick(Constants.JOYSTICK_DRIVER_ID);
  public static Joystick operatorStick = new Joystick(Constants.JOYSTICK_OPERATOR_ID);

  private final SendableChooser<Command> autoChooser = new SendableChooser<>();

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();

    autoChooser.addOption("Taxi", new Taxi());
    autoChooser.addOption("Shoot", new Shoot());
    autoChooser.addOption("Shoot Far", new ShootFar());
    autoChooser.addOption("Drive Shoot", new DriveShoot());
    autoChooser.addOption("Two Ball Auto Hanger Side", new TwoBallAutoHangerSide());
    autoChooser.addOption("Two Ball Auto Terminal Side", new TwoBallAutoTerminalSide());
    autoChooser.addOption("Three Ball Auto", new ThreeBallAuto());
    autoChooser.addOption("other", new TwoBallAutoTerminalSideWall());
    SmartDashboard.putData(autoChooser);
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {

    //shooter

    //shoot high
    new JoystickButton(operatorStick, Constants.SHOOTER_SHOOT_HIGH_BUTTON_ID)
    .whileHeld(new WaitShoot(false));

    //intake

    //intake lower and spin
    new JoystickButton(operatorStick, Constants.INTAKE_DEPLOY_AND_SPIN_BUTTON_ID)
    .whenPressed(new ArmDown(intakeArm))
    .whileHeld(new GetFromGutter())
    .whileHeld(new HorizontalIndexRun(horizontalIndex))
    .whileHeld(new VerticalIndexRun(verticalIndex, 0.3))
    .whenReleased(new HorizontalDoNothing(horizontalIndex))
    .whenReleased(new IntakeDoNothing(intakeWheels));

    //intake raise
    new JoystickButton(operatorStick, Constants.INTAKE_RAISE_BUTTON_ID)
    .whenPressed(new ArmUp(intakeArm));

    //indexer spin without arm down
    new JoystickButton(operatorStick, Constants.INDEX_SPIN_BUTTON_ID)
    .whileHeld(new HorizontalIndexRun(HorizontalIndex.getInstance()))
    .whileHeld(new VerticalIndexRun(VerticalIndex.getInstance()));


    //Reverse Everything
    new JoystickButton(operatorStick, Constants.REVERSE_EVERYTHING_BUTTON_ID)
    .whenHeld(new VerticalIndexSpit(VerticalIndex.getInstance()))
    .whenHeld(new HorizontalSpit(HorizontalIndex.getInstance()))
    .whenHeld(new IntakeWheelSpit(IntakeWheels.getInstance()));

    new JoystickButton(operatorStick, 8)
    .whileHeld(new DeployClimber(Climber.getInstance()));
    new JoystickButton(driverStick, 11)
    .whileHeld(new ResetClimber(Climber.getInstance()))
    .whileHeld(new DrivetrainDoNothing(drivetrain));

    new JoystickButton(operatorStick, 9)
    .whenPressed(new DeployClimberOnClick(Climber.getInstance()));

    new JoystickButton(driverStick, 9)
    .whileHeld(new ClimbWhileHeld(Climber.getInstance()));



  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    if(autoChooser.getSelected() != null) {
      return new WaitThanCommand(autoChooser.getSelected(), SmartDashboard.getNumber("Wait Time", 0));
    }
    return new DrivetrainDoNothing(Drivetrain.getInstance());
  }
}
